package fr.vxs.playlist;

public class Playlist {
    public int icon;
    public String title;
    public Playlist(){
        super();
    }
   
    public Playlist(int icon, String title) {
        super();
        this.icon = icon;
        this.title = title;
    }
}