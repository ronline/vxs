package fr.vxs.playlist;
//RH@C Webview class  

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.util.Log;

@SuppressLint("SetJavaScriptEnabled")
public class PlaylistScreen extends Activity {
	
	public static final String URL = "";
	private static final String TAG = "VxsActivityClass";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webscreen);
        
        String url = getIntent().getStringExtra(URL);
        Log.i(TAG, " URL = "+url);
        
        WebView webView = new WebView(this);
        //RH@C setting a WebChromeClient to support
        //online video streaming
        webView.setWebChromeClient(new WebChromeClient()); 
        //RH@C Overriding setting for javascript
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
		setContentView(webView);
		webView.loadUrl(url);
        
    }
}