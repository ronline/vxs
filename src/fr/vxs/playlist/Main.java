package fr.vxs.playlist;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;


public class Main extends Activity {

    private ListView PlayListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Playlist Playlist_data[] = new Playlist[]
        {
            new Playlist(R.drawable.promos, "Promos"),
            new Playlist(R.drawable.best_of, "Best Of"),
            new Playlist(R.drawable.divers, "Divers"),
            new Playlist(R.drawable.presentation, "Présentation"),
            new Playlist(R.drawable.trailer, "Trailers"),
            new Playlist(R.drawable.resultats, "Résultats"),
            new Playlist(R.drawable.reportage, "Emissions"),
            new Playlist(R.drawable.weight, "Pesées"),
            new Playlist(R.drawable.backstages, "Backstages")
        };
       
        PlaylistAdapter adapter = new PlaylistAdapter(this,
        R.layout.listview_item_row, Playlist_data);
        PlayListView = (ListView)findViewById(R.id.play_list);
        View header = (View)getLayoutInflater().inflate(R.layout.listview_header_row, null);
        PlayListView.addHeaderView(header);
        PlayListView.setAdapter(adapter);
    }
    /* 
     * RH@C Function 
     * 
     * Generate an email with possibility  
     * to choose an existing email client 
     *
     * @Param View
     */
    public void sendEMail(View v){
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
	    String[] mailto = new String[]{"vxs@hotmail.com", "",};
	    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, mailto);
	    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "VXS Mobile");
	    emailIntent.setType("text/html");
	    startActivity(Intent.createChooser(emailIntent, "Send mail client :"));
    }
    
    /*RH@C Function
     * 
     * Phone call intent 
     * Kick out the phone simulator interface
     * 
     * @Params View
     * */
    public void callTo(View v){
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:0033616799313"));
		startActivity(callIntent);
    }
    //End of RH@C

}