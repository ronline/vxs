package fr.vxs.playlist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PlaylistAdapter extends ArrayAdapter<Playlist> implements OnClickListener{

    Context context;
    int layoutResourceId;   
    Playlist data[] = null;
   
    public PlaylistAdapter(Context context, int layoutResourceId, Playlist[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PlaylistHolder holder = null;
       
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
           
            holder = new PlaylistHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
            holder.txtTitle.setOnClickListener(this);
            row.setTag(holder);
        }
        else
        {
            holder = (PlaylistHolder)row.getTag();
        }
       
        Playlist Playlist = data[position];
        holder.txtTitle.setText(Playlist.title);
        holder.imgIcon.setImageResource(Playlist.icon);
       
        return row;
    }
    /*RH@C  
     * Conditional statements evaluate the clicked 
     * Text view value and call the switchScreen method
     * with appropriate url parameter
     */
    @Override
    public void onClick(View v) {
    	TextView t = (TextView)v.findViewById(R.id.txtTitle);
    	if(t.getText().toString().equals("Promos")){
    		switchScreen("http://www.youtube.com/playlist?list=PL99F1006FB4B7EF79");
		} else if(t.getText().toString().equals("Best Of")){
			switchScreen("http://www.youtube.com/playlist?list=PL8BF20154F23B54CC");
		} else if(t.getText().toString().equals("Backstages")){
			switchScreen("http://www.youtube.com/playlist?list=PL26346C8081D2EEA6");
		} else if(t.getText().toString().equals("Présentation")){
			switchScreen("http://www.youtube.com/playlist?list=PLF35339875FC2531D");
		} else if(t.getText().toString().equals("Trailers")){
			switchScreen("http://www.youtube.com/playlist?list=PL99F1006FB4B7EF79");
		} else if(t.getText().toString().equals("Résultats")){
			switchScreen("http://www.youtube.com/playlist?list=PL434FA5DA3C5464C6");
		} else if(t.getText().toString().equals("Emissions")){
			switchScreen("http://www.youtube.com/playlist?list=PL5CB3FAB110C562D9");
		} else if(t.getText().toString().equals("Pesées")){
			switchScreen("http://www.youtube.com/playlist?list=PL8E0F80635A983CF8");
		} else if(t.getText().toString().equals("Divers")){
			switchScreen("http://www.youtube.com/playlist?list=PL8BF20154F23B54CC");
		}
    }
    

    
   /* RH@C FUNCTION 
    * Redirecting user to playlist
    *
    * @Param String link to youtube playlists
    * */
    private void switchScreen(String link){
		Intent j = new Intent(context, PlaylistScreen.class);
		j.putExtra(fr.vxs.playlist.PlaylistScreen.URL, link);
		context.startActivity(j);    	
    }
   //End of RH@C 
    static class PlaylistHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}